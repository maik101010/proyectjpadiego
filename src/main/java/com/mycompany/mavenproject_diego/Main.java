/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject_diego;

import com.mycompany.entidad.Ejercicio;
import com.mycompany.entidad.Respuesta;
import com.mycompany.entidad.Unidad;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Michael García A
 */
public class Main {

    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("miConexion");

    public static void main(String[] args) {

        crearDatos();
        //mostrarDatos();
        //List<Object> lista = mostrarDatosJoin();
        List<Object> lista = mostrarDatosJoin();
        System.out.println("------------LISTA-------------");
        for (Object object : lista) {
            System.out.println(object.toString());
        }
        
       
    }

    static void crearDatos() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Unidad unidad1 = new Unidad("Uso de procesador de texto");
        Unidad unidad2 = new Unidad("Uso de planilla de cálculo");
        /**
         * Guardar el valor en la base de datos
         */
        em.persist(unidad1);
        em.persist(unidad2);

        Ejercicio ejercicio1 = new Ejercicio("Ejercicio 1 de procesador de text", "Respuesta 1 procesador texto", 3, unidad1);
        Ejercicio ejercicio2 = new Ejercicio("Ejercicio 2 de procesador de texto ", "Respuesta 2 procesador texto", 4, unidad1);
        Ejercicio ejercicio3 = new Ejercicio("Ejercicio 3 de procesador de texto", "Respuesta 3 procesador texto", 5, unidad1);
        Ejercicio ejercicio4 = new Ejercicio("Ejercicio 4 de procesador de texto", "Respuesta 4 procesador texto", 5, unidad1);
        Ejercicio ejercicio5 = new Ejercicio("Ejercicio 1 de planilla de calculo", "Respuesta 1 planilla de calculo", 3, unidad2);
        Ejercicio ejercicio6 = new Ejercicio("Ejercicio 2 de planilla de calculo", "Respuesta 2 planilla de calculo", 3, unidad2);
        Ejercicio ejercicio7 = new Ejercicio("Ejercicio 3 de planilla de calculo", "Respuesta 3 planilla de calculo", 3, unidad2);
        Ejercicio ejercicio8 = new Ejercicio("Ejercicio 4 de planilla de calculo", "Respuesta 4 planilla de calculo", 3, unidad2);

        em.persist(ejercicio1);
        em.persist(ejercicio2);
        em.persist(ejercicio3);
        em.persist(ejercicio4);
        em.persist(ejercicio5);
        em.persist(ejercicio6);
        em.persist(ejercicio7);
        em.persist(ejercicio8);

        Respuesta respuesta1 = new Respuesta("Luis", "Respuesta 1 procesador texto", ejercicio1);
        Respuesta respuesta2 = new Respuesta("Luis", "Respuesta 2 procesador texto", ejercicio2);
        Respuesta respuesta3 = new Respuesta("Pedro", "Respuesta 3 procesador texto", ejercicio3);
        Respuesta respuesta4 = new Respuesta("Pedro", "Respuesta 4 procesador texto", ejercicio4);

        Respuesta respuesta5 = new Respuesta("Luis", "Respuesta 1 planilla de calculo", ejercicio5);
        Respuesta respuesta6 = new Respuesta("Luis", "Respuesta 2 planilla de calculo", ejercicio6);
        Respuesta respuesta7 = new Respuesta("Pedro", "Respuesta 3 planilla de calculo", ejercicio7);
        Respuesta respuesta8 = new Respuesta("Pedro", "Respuesta 4 planilla de calculo", ejercicio8);

        em.persist(respuesta1);
        em.persist(respuesta2);
        em.persist(respuesta3);
        em.persist(respuesta4);
        em.persist(respuesta5);
        em.persist(respuesta6);
        em.persist(respuesta7);
        em.persist(respuesta8);
        
        em.getTransaction().commit();

        em.close();
    }

    static void mostrarDatos() {
        EntityManager em = emf.createEntityManager();
        Unidad unidad = em.find(Unidad.class, 1);
        Ejercicio ejercicioOb = em.find(Ejercicio.class, 5);
        List<Ejercicio> ejercicios = unidad.getEjercicio();
        List<Respuesta> respuestas = ejercicioOb.getRespuestas();
        System.out.println("La unidad es " + unidad);
        System.out.println("************------ Unidades pertenecientes al ejercicio--------******");
        for (Ejercicio ejercicio : ejercicios) {
            System.out.println("*" + ejercicio.toString());
        }
        System.out.println("************------ Ejercicios pertenecientes a la respuesta--------******");
        for (Respuesta respuesta : respuestas) {
            System.out.println("*" + respuesta.toString());
        }

        em.close();

    }

    static List<Object> mostrarDatosJoin() {
        EntityManager em = emf.createEntityManager();
//        String query = "select alumno, count(*) as cantidad  from respuesta\n" +
//                        "inner join ejercicio on respuesta.Ejercicio_idEjercicio = ejercicio.idEjercicio\n" +
//                        "where ejercicio.Unidad_idUnidad\n" +
//                        "in (select idUnidad from unidad\n" +
//                        "where tema <> "+"'uso de procesador de texto')"+
//                        " group by alumno\n" +
//                        "order by cantidad desc";   
//        
        String query1 = "select * from respuesta";
        //List<Object> listResultados = em.createQuery(query).getResultList();    
        //Query q = em.createNativeQuery(query1);
        List<Object> listResultados =em.createNativeQuery(query1).getResultList();
        //List<Respuesta> listResultados = (List<Respuesta>)q.getResultList();
        //List<Object> listResultados = q.getResultList();
        return listResultados;

    }

}
