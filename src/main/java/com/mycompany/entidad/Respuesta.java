/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.ManyToAny;

/**
 *
 * @author Michael García A
 */
@Entity()
@Table(name = "Respuesta")
public class Respuesta {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idRespuesta")
    private int id;
    @Column(name = "alumno")
    private String alumno;
    @Column(name = "respuesta")
    private String respuesta;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Ejercicio_idEjercicio")
    private Ejercicio ejercicio;

    public Respuesta() {
    }

    public Respuesta(String alumno, String respuesta, Ejercicio ejercicio) {
        this.alumno = alumno;
        this.respuesta = respuesta;
        this.ejercicio = ejercicio;
    }

    public String getAlumno() {
        return alumno;
    }

    public void setAlumno(String alumno) {
        this.alumno = alumno;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Ejercicio getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(Ejercicio ejercicio) {
        this.ejercicio = ejercicio;
    }

    @Override
    public String toString() {
        return "Respuesta{" + "alumno=" + alumno + ", respuesta=" + respuesta + ", ejercicio=" + ejercicio + '}';
    }
    
    
    
    
}
