/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.entidad;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Michael García A
 */
@Entity()
@Table(name = "Ejercicio")
public class Ejercicio {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idEjercicio")
    private int id;
    @Column(name = "consigna")
    private String consigna;
    @Column(name = "respuesta")
    private String respuesta;
    @Column(name = "puntaje")
    private int puntaje;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Unidad_idUnidad")
    private Unidad unidad;
    
    
    @OneToMany(mappedBy = "ejercicio", cascade = CascadeType.ALL)
    private List<Respuesta> respuestas = new ArrayList<>();

    public Unidad getUnidad() {
        return unidad;
    }

    public void setUnidad(Unidad unidad) {
        this.unidad = unidad;
    }

    public List<Respuesta> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(List<Respuesta> respuestas) {
        this.respuestas = respuestas;
    }
    
    

    public Ejercicio() {
    }

    public Ejercicio(String consigna, String respuesta, int puntaje, Unidad unidad) {
        this.respuesta = respuesta;
        this.consigna = consigna;
        this.puntaje = puntaje;
        this.unidad = unidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConsigna() {
        return consigna;
    }

    public void setConsigna(String consigna) {
        this.consigna = consigna;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    @Override
    public String toString() {
        return "Ejercicio{" + "consigna=" + consigna + ", respuesta=" + respuesta + ", puntaje=" + puntaje + ", unidad=" + unidad + '}';
    }

}
