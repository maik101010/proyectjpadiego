/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.entidad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Michael García A
 */
@Entity()
@Table(name = "Unidad")
public class Unidad implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idUnidad")
    private int id;
    @Column(name = "tema")
    private String tema;
    
    @OneToMany(mappedBy = "unidad",  cascade = CascadeType.ALL)    
    private List<Ejercicio> ejercicio = new ArrayList<>();    

    public List<Ejercicio> getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(List<Ejercicio> ejercicio) {
        this.ejercicio = ejercicio;
    }

    public Unidad() {
    }

    public Unidad(String tema) {
        this.tema = tema;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    @Override
    public String toString() {
        return "Unidad{" + "id=" + id + ", tema=" + tema + '}';
    }

   
    
}
